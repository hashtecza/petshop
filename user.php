<?php
	ini_set('display_errors', 'On');
	error_reporting(E_ALL);

    $apptitle = "user";

    $appstyles = '
    	<link type="text/css" rel="Stylesheet" href="resources/css/stylesheet.css">
    ';

    $appcontent = "
    <div class=\"sign-in-container\">
    	<h1 class=\"sign-in\"> <i class=\"fa fa-user\"></i> Sign In </h1>
    	<div class=\"sign-in-box\">
    		<form id=\"login-form\" action=\"app/sentinel/login.php\" method=\"POST\">
    			<div class=\"username\">
    				<p class=\"sign-in-details\"> Username: </p><input type=\"text\" class=\"form-control\" name=\"username\" placeholder=\"Insert username here...\" aria-describedby=\"sizing-addon5\">
    			</div>

    			<div class=\"email\">
    				<p class=\"sign-in-details\"> Email: </p> <input type=\"text\" class=\"form-control\" name=\"email\" placeholder=\"Insert email here...\">
    			</div>

    			<div class=\"password\">
    				<p class=\"sign-in-details\"> Password: </p> <input type=\"password\" class=\"form-control\" name=\"password\" placeholder=\"Insert password here...\">
    			</div>
    			<br/>
    		</form>
    	</div>
    	<div class=\"sign-in-send\">
    		<input type=\"submit\" value=\"Sign In\" class=\"sign-in-send-button\" onclick=\"document.getElementById('login-form').submit();\" >
    	</div>
    </div>

    <div class=\"register-container\">
    	<h1 class=\"register\"> <i class=\"fa fa-user-plus\"></i> Register </h1>
    	<div class=\"register-box\">
    		<form id=\"register-form\" action=\"app/sentinel/register.php\" method=\"POST\">
    			<div class=\"name\">
    			<p class=\"sign-in-details\"> Name: </p> <input type=\"text\" class=\"form-control\" name=\"name\" placeholder=\"Insert name here...\">
    			</div>

    			<div class=\"surname\">
    				<p class=\"sign-in-details\"> Surname: </p> <input type=\"text\" class=\"form-control\" name=\"surname\" placeholder=\"Insert surname here...\">
    			</div>

    			<div class=\"username\">
    				<p class=\"sign-in-details\"> Username: </p> <input type=\"text\" class=\"form-control\" name=\"username\" placeholder=\"Insert username here...\">
    			</div>

    			<div class=\"register-email\">
    				<p class=\"sign-in-details\"> Email: </p> <input type=\"text\" class=\"form-control\" name=\"email\" placeholder=\"Insert email here...\">
    			</div>

    			<div class=\"register-password\">
    				<p class=\"sign-in-details\"> Password: </p> <input type=\"password\" class=\"form-control\" name=\"password\" placeholder=\"Insert password here...\">
    			</div>
    			<br/>
    		</form>
    	</div>
    	<div class=\"register-send\">
    		<input type=\"submit\" value=\"Register\" class=\"register-send-button\" onclick=\"document.getElementById('register-form').submit();\">
    	</div>
    </div>
	";

	$appscripts = "
		<!--JavaScript-->
		<script src=\"node_modules/jquery/dist/jquery.js\"></script>
		<script src=\"node_modules/bootstrap/dist/js/bootstrap.js\"></script>
		<script src=\"resources/loud-links-master/loudlinks.min.js\"></script>
		<script src=\"resources/js/scriptsheet.js\"></script>
	";

	include "layout.php";
?>