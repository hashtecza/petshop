<?php

include_once (dirname(__DIR__) . "/db_conn.php");
include_once (dirname(__DIR__, 2) . "/boot.php");

$user = $sentinel::check();
if (! $user) die(); //TODO catch exepion

$wishlist_retrieve_query = $conn->prepare(
	'SELECT products.*, wishlists.products_id, wishlists.user_id from `wishlists` 
	INNER JOIN products
	ON wishlists.products_id = products.id
	WHERE user_id = :user_id
	');

$wishlist_retrieve_query->execute([
	':user_id' => $user->id
]);

$wishlist_retrieve_results = $wishlist_retrieve_query->fetchAll(PDO::FETCH_ASSOC);

?>