<?php 
include "../db_conn.php";

$name = $_POST['name'];
$price = $_POST['price'];
$description = $_POST['message'];

$product_create_query = $conn->prepare("INSERT INTO products (name,price,description)
	VALUES (:name, :price, :description)");

$product_create_query->execute([
	":name" => $name,
	":price" => $price,
	":description" => $description,
	]);

$_SESSION['message'] = [
'body' => "You have created $name",
'type' => "success"
];

header('Location: ' . $_SERVER['HTTP_REFERER']);
die();
