<?php 
include "../db_conn.php";

$id = $_POST['id'];
$name = $_POST['name'];
$price = $_POST['price'];
$description = $_POST['message'];

$product_update_query = $conn->prepare("UPDATE products SET name = :name, price = :price, description = :description WHERE `products`.id = :id;");

$product_update_query->execute([
	":id" => $id,
	":name" => $name,
	":price" => $price,
	":description" => $description,
	]);

$_SESSION['message'] = [
'body' => "You have updated $name",
'type' => "success"
];

header( 'Location: http://localhost/Petshop/backend/retrieve.php' );
die();