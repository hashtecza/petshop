<?php 
include_once (dirname(__DIR__) . '..\..\db_conn.php');

$adultfood_of_cats_retrieve = $conn->prepare("
SELECT products.id, products.name, products.price, products.description, products_animals.name as animal_name, products_category.name as category_name, animals_sizes.name as animal_size FROM `products`   
INNER JOIN products_animals 
ON products.animal_id = products_animals.id
INNER JOIN products_category
ON products.category_id = products_category.id 
INNER JOIN animals_sizes
ON products.animal_size_id = animals_sizes.id
WHERE products_animals.id=1 and animals_sizes.id=1,2
");

$adultfood_of_cats_retrieve->execute();

$q = $adultfood_of_cats_retrieve->fetchAll(PDO::FETCH_ASSOC);