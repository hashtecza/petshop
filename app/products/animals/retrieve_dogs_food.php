<?php 
include_once (dirname(__DIR__) . '..\..\db_conn.php');

$food_of_dogs_retrieve = $conn->prepare("
SELECT products.id, products.name, products.price, products.description, products_animals.name as animal_name, products_category.name as category_name FROM `products`   
INNER JOIN products_animals 
ON products.animal_id = products_animals.id
INNER JOIN products_category
ON products.category_id = products_category.id
WHERE products_animals.id=1 AND products_category.id = 2
");

$food_of_dogs_retrieve->execute();

$q = $food_of_dogs_retrieve->fetchAll(PDO::FETCH_ASSOC);