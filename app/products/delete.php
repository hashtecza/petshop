<?php 
include "../db_conn.php";

$id = $_POST['id'];

$product_delete_query = $conn->prepare("DELETE FROM products WHERE id= :id");

$product_delete_query->execute([
	":id" => $id
	]);

$_SESSION['message'] = [
'body' => "Successfully deleted.",
'type' => "success"
];

header('Location: ' . $_SERVER['HTTP_REFERER']);
die();