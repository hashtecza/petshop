<?php

$host = "localhost";
$dbname = "petshop";
$username = "root";
$password = "";

$pdo = new PDO("mysql:host=$host;dbname=$dbname", $username, $password);

$sql = 'SELECT id, name, price, description
        FROM products
        ORDER BY price ASC';

$q = $pdo->query($sql)->fetchAll(PDO::FETCH_ASSOC);
