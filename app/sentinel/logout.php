<?php

	require_once('initialize.php');

	$user = $sentinel::check();

	if ($user) {
		$sentinel::logout($user, true);
	} else {
		$_SESSION['message'] = [
			'body' => "User was already not logged in.",
			'type' => "danger"
		]; 

		;
		header( 'Location: http://localhost/Petshop/home.php' );
		die();
	}

	$_SESSION['message'] = [
		'body' => "You have been logged out.",
		'type' => 'success'
	];
	header( 'Location: http://localhost/Petshop/home.php' );