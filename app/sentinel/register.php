<?php

	require_once ('initialize.php');

	//TODO:: handle validation.

	$username = $_POST['username'];
	$name = $_POST['name'];
	$surname = $_POST['surname'];
	$email = $_POST['email'];
	$username = $_POST['username'];
	$password = $_POST['password'];

	$credentials = [
		'first_name' => $name,
		'last_name' => $surname,
		'email' => $email,
		'password' => $password
	];

	$credentialsValidated = [
		'login' => $email
	];

	$registered_user = $sentinel::findByCredentials($credentialsValidated);

	if ($registered_user) {
		$_SESSION['message'] = [
		'body' => "User already registered.",
		'type' => "danger"
		];

		header( 'Location: http://localhost/Petshop/home.php' );
		die();
	}

	$user = $sentinel::registerAndActivate($credentials);
	//TODO:: HANDLE ACTIVATION SEPERATELY (WE WANT AN EMAIL)

	if (!$user) {
		$_SESSION['message'] = [
		'body' => "User must register.",
		'type' => "danger"
		];

		header( 'Location: http://localhost/Petshop/home.php' );
		die();
	}
	
	$user->name =$username;
	$user->save ();

	$_SESSION['message'] = [
		'body' => "You have registered.",
		'type' => "success"
	];

	header( 'Location: http://localhost/Petshop/home.php' );
	die();
?>