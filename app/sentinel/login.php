<?php

	require "initialize.php";

	$email = $_POST['email'];
	$password = $_POST['password'];

	$credentials = [
		'email'    => $email,
		'password' => $password,
	];

	$user = $sentinel::authenticate($credentials);

	if (!$user) {
		$_SESSION['message'] = [
		'body' => "User must log in.",
		'type' => "danger"
		];

		header( 'Location: http://localhost/Petshop/home.php' );
		die();
	}
	$sentinel::login($user);

	$_SESSION['message'] = [
		'body' => "You have logged in.",
		'type' => 'success'
	];

	header( 'Location: http://localhost/Petshop/home.php' );
	die();
	?>