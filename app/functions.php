<?php 

include_once ('db_conn.php');

	function addRetrievedProducts($q) {
		$result = '';
		foreach ($q as $product)
		{
			$result .= '
					<div class="col-md-3">
						<div class="thumbnail">
							<div data-toggle="modal" data-target="#myModal">
								<div class="product" id="product-' . $product['id'] . '" ' . getUserWishlist($product['id']) . '>
									<b> ' . $product['name'] . '</b>
										<p class="product_price">
											R' . $product['price'] . '
										</p>
									<img id="testimage" class="placeholder loader" src="resources/images/ajax-loader.gif" alt="loading"> 
									<p class="caption"> 
										' . $product['description'] . '
									</p>
								</div>
							</div> <!-- end of modal -->
						</div> <!--end of thumbnail-->
					</div> <!--end of col-->
			';
		}
		return $result;
	}

	function getUserWishlist($id) {
		global $user, $conn;
		if (! $user) { return ""; }

		if (! $conn) {
			//TODO:: catch exception
			return '';
		}
		
		$user_wishlist_retrieve_query = $conn->prepare(
			"SELECT products.* FROM wishlists 
			INNER JOIN products
			ON wishlists.products_id = products.id
			WHERE user_id = :user_id"
		);

		$user_wishlist_retrieve_query->execute([
			':user_id' => $user->id,
		]);

		$user_wishlist_retrieve_result = $user_wishlist_retrieve_query->fetchAll(PDO::FETCH_ASSOC);

		foreach ($user_wishlist_retrieve_result as $product) {
			if ($id == $product['id']) {
				return ' data-wishlist="true"';
			} 
		}
		return ''; // false
	}

	function getWishlist() {
		global $user;
		if (! $user) { return ''; }
		return "
			<div class=\"wishlist\">
				<i class=\"fa fa-heart-o\"></i>
					Add to wishlist
			</div>";
	}

	function getCheckout() {
		global $user;
		if (! $user) { return ''; }
		return "
			<div class=\"checkout\">
				<i class=\"fa fa-shopping-cart\"></i>
					Add to cart
			</div>";
	}

	function getWishlistJavascript() {
		return "
		$('.wishlist').click(function() {
				var wishlist_number = parseInt($('#wishlist-content-number').html());
				if (! $('.fa').hasClass('redheart')) {
					$.post('app/wishlists/create.php',
					{
						products_id: $('.modal-content').attr('id'),
						user_id: " . getUserId() . "
					},
					function(data, status) {

						console.log(data);
						if (status == 'success') {
							wishlist_number++;
							$('#wishlist-content-number').html(wishlist_number);
							$('.wishlist .fa').addClass('redheart').addClass('fa-heart').removeClass('fa-heart-o');
						}
					});
				} else {
					$.post('app/wishlists/delete.php',
					{
						products_id: $('.modal-content').attr('id'),
						user_id: " . getUserId() . "
					},
					function(data, status) {
						console.log(data);
						if (status == 'success') {
							wishlist_number--;
							$('#wishlist-content-number').html(wishlist_number);
							$('.wishlist .fa').removeClass('redheart').removeClass('fa-heart').addClass('fa-heart-o');
						}
					});
				}
			});";
	}

	function getUserId() {
		global $user;
		if (! $user ) { return ''; }
		return $user->id;
	}
?>