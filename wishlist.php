<?php
	ini_set('display_errors', 'On');
	error_reporting(E_ALL);

	include "app/wishlists/retrieve.php";
	include 'app/functions.php';

    $apptitle = "wishlist";

    $appstyles = '
    	<link type="text/css" rel="Stylesheet" href="resources/css/stylesheet.css">
    ';

    $appcontent = "
    	<div id=\"wishlist-box\">
			<h2 class=\"heading2\">Wishlist</h2>
				<div class=\"wishlist-content\"> 
					<table class='table table-hover'>
						<thead>
							<tr>
								<th>Name</th>
								<th>Price</th>
								<th> </th>
							</tr>
						</thead>
						<tbody>
							" . getWishlistItems() . "
						</tbody>
					</table>
				</div>
		</div>
	";

	$appscripts = "
	";

	include "layout.php";

	function getWishlistItems() {
		global $wishlist_retrieve_results;
		$results = '';

		if (! $wishlist_retrieve_results) return '';

		foreach ($wishlist_retrieve_results as $item) {
			$results .= 
				'<tr>
					<td>' . $item['name'] . '</td>
					<td> R ' . $item['price'] . '</td>
					<td>
						<form action="http://localhost/petshop/app/wishlists/delete.php" method="POST">
							<input type="hidden" name="products_id" value="' . $item['products_id'] . '">
							<input type="hidden" name="user_id" value="' . $item['user_id'] . '">
							<i class="fa fa-trash" onclick=\'$(this).closest("form").submit();\'>
						</form>
					</td>
				</tr>';
		}
		return $results;
	}
?>