<div class="sidebar">
    <nav class="sidebar-nav">
        <ul class="nav">
            <li class="nav-item">
                <a class="nav-link" href="index.html"><i class="icon-speedometer"></i> Dashboard <span class="badge badge-info">NEW</span></a>
            </li>
            <li class="divider"></li>
            <li class="nav-item nav-dropdown">
            <a class="nav-link nav-dropdown-toggle" href="#"><i class="fa fa-tag"></i> Products</a>
                <ul class="nav-dropdown-items">
                    <li class="nav-item">
                        <a class="nav-link" href="./create.php"><i class="fa fa-plus"></i> Create</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="./retrieve.php"><i class="fa fa-folder-open"></i> Retrieve</a>
                    </li>
                </ul>
            </li>
            <?php echo $appnav; ?>
        </ul>
    </nav>
</div>