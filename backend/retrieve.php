<?php 
include "../app/products/retrieve.php";

// var myUrl =

$apptitle = "retrieve";

$appstyles = '';

$appnav = '';

$appcontent = "
 	<table class='table table-hover'>
		<tr>
			<th> Name </th>
			<th> Price </th> 
			<th> Description </th>
		</tr>
		" . getProducts($q) . "
	</table>	
";

$appscripts = '';

include "backend-layout.php";

function getProducts($q) {
	$result = '';
	foreach ($q as $product)
	{	
		$result.= '
		<tr>
			<td> ' . $product['name'] . ' </td> 
			<td> ' . $product['price'] . ' </td> 
			<td> ' . $product['description'] . ' </td>
			<td> <a href="update.php?id=' . $product['id'] .'&name=' . $product['name'] . '&price=' . $product['price'] . '&message=' . $product['description'] . '" value=""><i class="fa fa-pencil"></td>
			<td>
				<form action="http://localhost/petshop/app/products/delete.php" method="POST">
					<input type="hidden" name="id" value="' . $product['id'] . '">
					<i class="fa fa-trash" onclick=\'$(this).closest("form").submit();\'>
				</form> 
			</td>
		</tr>
		';
		}
	return $result;
	}
?>
