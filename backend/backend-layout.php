<?php
	
	require_once('../boot.php');

	if ( ! empty($apptitle) ) {
		$apptitle = ' | ' . $apptitle;
	} else {
		$apptitle = '';
	}

	if ( !empty($appcontent) ) {
		$appcontent = $appcontent;
	} else {
		$appcontent = '';
	}

	if ( !empty($appscripts) ) {
		$appscripts = $appscripts;
	} else {
		$appscripts = '';
	}

	if ( !empty($appstyles) ) {
		$appstyles = $appstyles;
	} else {
		$appstyles = '';
	}

	if ( !empty($appnav) ) {
		$appnav = $appnav;
	} else {
		$appnav = '';
	}
?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="CoreUI - Open Source Bootstrap Admin Template">
    <meta name="author" content="Łukasz Holeczek">
    <meta name="keyword" content="Bootstrap,Admin,Template,Open,Source,AngularJS,Angular,Angular2,Angular 2,Angular4,Angular 4,jQuery,CSS,HTML,RWD,Dashboard,React,React.js,Vue,Vue.js">
    

    <title>PetShop | Backend<?php echo $apptitle; ?></title>
	
	<link rel="tabicon" href="/petshop/resources/images/dog_paw.png">
    
    <!-- Icons -->
    <link href="res/css/font-awesome.min.css" rel="stylesheet">
    <link href="res/css/simple-line-icons.css" rel="stylesheet">

    <!-- Main styles for this application -->
    <link href="res/css/style.css" rel="stylesheet">

    <?php echo $appstyles; ?>

</head>

<body class="app header-fixed sidebar-fixed aside-menu-fixed aside-menu-hidden">

	<header class="app-header navbar">
        <button class="navbar-toggler mobile-sidebar-toggler d-lg-none" type="button">☰</button>
        <a class="navbar-brand" href="#"></a>
    </header>

    <div class="app-body">
        <?php include "includes/backend-nav.php"; ?>
        <?php

		?>

		<main class="main">
            <div class="container-fluid">
                <div class="animated fadeIn">
                	<?php
                		if (isset($_SESSION['message'])) {
						echo '<div class="alert alert-' . getMessageType() . '">			
						' . $_SESSION['message']['body'] . '
						</div>';
						}
                	?>
					<?php echo $appcontent; ?>
                </div>
            </div> <!-- /.conainer-fluid -->
        </main>
    </div>

    <footer class="app-footer">
        Copyright © 2017. All Rights Reserved. <u> Website Created by Hashtec. </u> 
    </footer>


    <!-- Bootstrap and necessary plugins -->
	<script src="../node_modules/jquery/dist/jquery.js"></script>
	<script src="../node_modules/bootstrap/dist/js/bootstrap.js"></script>
    <script src="res/js/app.js"></script>

	<?php
		echo $appscripts;
	?>

</body>

</html>

<?php
	if (isset($_SESSION['message'])) {
		session_unset();
	}

	function getMessageType() {
		if (isset($_SESSION['message'])) {
			if (isset($_SESSION['message']['type'])) {
				return $_SESSION['message']['type'];
			}

			return 'info';
		}
	}
?>