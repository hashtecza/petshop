<?php

if(isset($_GET['message'])) {
	$description=$_GET['message'];
} else {
	$description="";
}

if(isset($_GET['price'])) {
	$price=$_GET['price'];
} else {
	$price="";
}

if(isset($_GET['name'])) {
	$name=$_GET['name'];
} else {
	$name="";
}

if(isset($_GET['id'])) {
	$id=$_GET['id'];
} else {
	$id="";
}

$apptitle = "update";

$appstyles = '';

$appnav = '';

$appcontent = '
<div class="col-md-4 offset-md-1">
	<div class="container">
		<form action="http://localhost/petshop/app/products/update.php" method="POST">
		<input type="hidden" name="id" value="' . $id . '">
  			<div class="form-group">
	    		<label for="exampleInputEmail1">Name</label>
	    		<input type="text" value="' . $name . '" name="name" class="form-control" placeholder="Insert Name here...">
  			</div>

  			<div class="form-group">
	    		<label for="exampleInputPassword1">Price</label>
	    		<input type="text" name="price" class="form-control" value="' . $price .'" placeholder="Insert Price here...">
  			</div>

			<div class="form-group">
    			<label for="exampleInputPassword1">Description</label>
  			</div>

			<div>
				<textarea name="message" rows="10" cols="60"  placeholder="Insert message here...">' . $description . '</textarea>
			</div>

  			<button type="submit" class="btn btn-default">Submit</button>
		</form>
	</div>
</div>
';

$appscripts = "";

include "backend-layout.php";
?>