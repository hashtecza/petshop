<?php 
$apptitle = "create";

$appstyles = "<link href=\"res/css/my-stylesheet.css\" rel=\"stylesheet\">";

$appnav = '';

$appcontent = '
<div class="col-md-5 offset-md-3">
	<div class="container">
		<form action="http://localhost/petshop/app/products/create.php" method="POST">
  			<div class="form-group">
  				<span class="fa fa-address-card-o"></span>
	    		<label for="exampleInputEmail1">Name:</label>
	    		<input type="text" name="name" class="form-control" placeholder="Insert Name here...">
  			</div>

  			<div class="form-group">
	    		<label for="exampleInputPassword1">Price:</label>
	    		<input type="text" name="price" class="form-control" placeholder="Insert Price here...">
  			</div>
			
			<div class="animal_types_sizes">
				<label for="exampleInputPassword1">Animal Type:</label>
		  			<select name="animal_types">
		    			<option value="dog">Dog</option>
					    <option value="cat">Cat</option>
					</select>

				<label for="exampleInputPassword1">Animal Size:</label>
		  			<select name="animal_sizes">
		    			<option value="dog_puppy">Puppy</option>
					    <option value="cat_kitten">Kitten</option>
					    <option value="adult">Adult</option>
					</select>
			</div>

			<div class="form-group">
    			<label for="exampleInputPassword1">Description:</label>
  			</div>

			<div>
				<textarea name="message" rows="10" cols="60" placeholder="Insert message here..." class="form-control"> 
				</textarea>
			</div>

  			<button type="submit" class="btn btn-default">Submit</button>
		</form>
	</div>
</div>
';

$appscripts = "";

include "backend-layout.php";
?>