<ul class="nav"> 
	<li role="presentation" class="active">
		<a href="home.php"><i class="fa fa-home"></i> Home</a>
	</li>
	
	<li role="presentation" class="dropdown"> 
		<a href="dogs.php" class="dropdown-toggle loud-link-hover" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" data-sound="dog_barking"><i class="fa fa-paw"></i> Dogs <span class="caret"></span></a>
			<ul class="dropdown-menu">
				 <li>
					<a href="dog_food.php"> Dog Food </a>
				</li>
				<li role="presentation" class="dropdown dropdown-submenu">
					<a href="dog_types.php" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> Dog Types </a>
					<hr>
					<ul class="dropdown-menu">
						<li>
							<a href="puppies.php"> Puppy </a>
						</li>
						<li>
							<a href="dog_adult.php"> Adult </a>
						</li>
					</ul>
				</li>
				
			</ul>

			<li role="presentation" class="dropdown"> 
				<a href="cats.php" class="dropdown-toggle loud-link-hover" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" data-sound="cat_meow"><i class="fa fa-paw"></i> Cats <span class="caret"></span></a>
				<ul class="dropdown-menu">
					<li>
						<a href="cat_food.php">Cat Food</a>
					</li> 
					<li role="presentation" class="dropdown dropdown-submenu">
						<a href="cat_types.php" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Cat Types</a>
						<ul class="dropdown-menu">
							<li>
								<a href="kittens.php"> Kitten </a>
							</li>
							<li>
								<a href="cat_adult.php"> Adult </a>
							</li>
						</ul>
					</li> 
				</ul>
			</li>

	<li role="presentation" class="active">
		<a href="contact_us.php"><i class="fa fa-phone"></i> Contact Us</a>
	</li>

	<li role="presentation" class="dropdown"> 
		<a href="cats.php" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-search"></i> Search Here <span class="caret"></span></a>
		<ul class="dropdown-menu dropdown-search">
			<li>
				<div class="input-group">
					<span class="input-group-addon" id="basic-addon1">
						<img class="search-image" src="resources/images/search.png" alt="search-image">
					</span>
					<input type="text" name="searchbox" class="search" placeholder="Search Here...">
				</div>
			</li> 
		</ul>
	</li>

		<?php
			$user = $sentinel::check();
			if (! $user) {
				echo '';
			} else {
				echo '
				<li role="presentation" class="active">
					<a href="wishlist.php"><i class="fa fa-heart"></i> Wishlist (<span id="wishlist-content-number">' . getWishlistCount() . '</span>)</a>
				</li>
				';
			}

			if (! $user) {
				echo '';
				
			} else {
				echo '
				<li role="presentation" class="active">
					<a href="checkout.php"><i class="fa fa-shopping-bag"></i> Checkout</a>
				</li>
				';
			}

			if (! $user) {
				echo '
				<li role="presentation" class="active">
					<a href="user.php"><i class="fa fa-user"></i>Sign In | <i class="fa fa-user-plus"></i>Register</a>
				</li> 
				';
			} else {
				echo '
				<li role="presentation">
				    <a href="app/sentinel/logout.php">
				    	Logout
			    	</a>
				</li>
				<li role="presentation">
					<div class="guest">
					    Hello, ' . $user->name . '
					</div>
				</li>';
			}

			function getWishlistCount() {
				global $user, $conn;

				if (! $user) { return ''; } 

				if (! $conn) { return 'error'; }

				$user_wishlist_count_query = $conn->prepare("SELECT count(id) as count FROM wishlists WHERE user_id = :user_id");

				$user_wishlist_count_query->execute([
					"user_id" => $user->id
				]);

				$user_wishlist_count_result = $user_wishlist_count_query->fetchAll(PDO::FETCH_ASSOC);

				// print_r($user_wishlist_count_result);
				if ($user_wishlist_count_result[0]['count'] == null OR $user_wishlist_count_result[0]['count'] == 0) { return '0'; }
				return $user_wishlist_count_result[0]['count'];
			}
		?>
	
</ul>