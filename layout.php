<?php

	require_once('boot.php');
	include_once('app/db_conn.php');

	if ( ! empty($apptitle) ) {
		$apptitle = ' | ' . $apptitle;
	} else {
		$apptitle = '';
	}

	if ( !empty($appcontent) ) {
		$appcontent = $appcontent;
	} else {
		$appcontent = '';
	}

	if ( !empty($appscripts) ) {
		$appscripts = $appscripts;
	} else {
		$appscripts = '';
	}

	if ( !empty($appstyles) ) {
		$appstyles = $appstyles;
	} else {
		$appstyles = '';
	}

?>

<!DOCTYPE html>
<html lang="en">
<head>

	<meta charset="UTF-8">
	<meta name="description" content="Free Web tutorials">
	<meta name="keywords" content="HTML,CSS,XML,JavaScript">
	<meta name="author" content="Hashtec">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<title>Petshop<?php echo $apptitle; ?></title>

	<link rel="icon" href="resources/images/dog_paw.png">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

	<link rel="stylesheet" href="node_modules/bootstrap/dist/css/bootstrap.css">

	<link type="text/css" rel="Stylesheet" href="resources/css/stylesheet.css">
	
	
</head>
<body>
	
	<div class="wrapper">
		<?php  include "includes/nav.php"; ?>
		<?php
			if (isset($_SESSION['message'])) {
				echo '<div class="alert alert-' . getMessageType() . '">
		  			' . $_SESSION['message']['body'] . '
				</div>';
			}
		?>
		<?php echo $appcontent; ?>
	</div>
	
	

	<div id="footer"> Copyright 
		<img class="copyright" src="resources/images/copyright.jpg" alt="copyright"> 2017. All Rights Reserved. <u> Website Created by Hashtec. </u> 
	</div>

<!-- scripts will be here -->
<script src="node_modules/jquery/dist/jquery.js"></script>
<script src="node_modules/bootstrap/dist/js/bootstrap.js"></script>
<script src="resources/loud-links-master/loudlinks.min.js"></script>
<script src="resources/Js/Scriptsheet.js"></script>

<?php
	echo $appscripts;
?>


</body> <!--end of body-->
</html>

<?php
	if (isset($_SESSION['message'])) {
		unset($_SESSION['message']);
	}

	function getMessageType() {
		if (isset($_SESSION['message'])) {
			if (isset($_SESSION['message']['type'])) {
				return $_SESSION['message']['type'];
			}

			return 'info';
		}
	}
?>