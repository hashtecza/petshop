<?php 
	$title = 'Contact Us';
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="description" content="Free Web tutorials">
	<meta name="keywords" content="HTML,CSS,XML,JavaScript">
	<meta name="author" content="Hashtec">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<title><?php echo $title; ?></title>

	<link rel="icon" href="resources/Images/dog_paw.png">
	<link rel="stylesheet" href="node_modules/bootstrap/dist/css/bootstrap.css">
	<link rel="stylesheet" href="node_modules/leaflet/dist/leaflet.css">
	<link type="text/css" rel="Stylesheet" href="resources/Css/Stylesheet.css">
</head>

<body>
	<ul class="nav"> 
		<li role="presentation" class="active">
			<a href="Home.html">Home</a>
		</li>
	
		<li role="presentation" class="dropdown"> 
			<a href="Dogs.html" class="dropdown-toggle loud-link-hover" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" data-sound="Dog_Barking"> Dogs <span class="caret"></span></a> 
				<ul class="dropdown-menu"> 
					<li>
						<a href="Dog_Types.html"> Dog Types </a>
					</li>
					<li>
						<a href="Dog_Food.html"> Dog Food </a>
					</li>
				</ul>
		
		<li role="presentation" class="dropdown"> 
			<a href="Cats.html" class="dropdown-toggle loud-link-hover" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" data-sound="Cat_Meow"> Cats <span class="caret"></span> </a> 
				<ul class="dropdown-menu"> 
					<li>
						<a href="Cat_Types.html">Cat Types</a>
					</li> 
					<li>
						<a href="Cat_Food.html">Cat Food</a>
					</li> 
 				</ul>
 		
 		<li role="presentation" class="active">
 			<a href="Contact_Us.html">Contact Us</a>
 		</li>  
 		
 		<li>
 			<div class="input-group">
  				<span class="input-group-addon" id="basic-addon1">
  					<img class="search-image" src="resources/Images/Search.png" alt="search-image">
  				</span>
					<input type="text" name="searchbox" class="search" placeholder="Search Here...">
			</div>
		</li> 
	</ul>

	<h2 class="heading2"> 
		<img class="pet-icons" src="resources/Images/cat_paw.png" alt="pet-icons"> 
			Contact Us 
		<img class="pet-icons" src="resources/Images/cat_paw.png" alt="pet-icons"> 
	</h2>

<div class="container">
	<div id="contact-box">
		<p class="Contact-Details"> Cell: 083 145 2842 </p>
		<p class="Contact-Details"> Email: MyPet@pet.mail </p>
		<p class="Contact-Details"> Address: 51 Smith Street </p>
	</div>
		<div id="mapId"> </div>
<!-- Feedback -->			
	<h2 class="heading2"> Feedback </h2>
	<div id="feedback-box">
		<form action="/Petshop/app/feedback.php" method="GET">
			<p class="feedback-details"> Name </p> <input type="text" name="name" placeholder="Insert name here...">
			<p class="feedback-details"> Email </p> <input type="text" name="email" placeholder="Insert email here...">
			<p class="feedback-details"> Message </p> <textarea name="message" rows="10" cols="40" placeholder="Insert message here..."></textarea> <br/>
		<input type="submit" value="Send" class="send-button"><input type="reset" value="Clear" class="clear-button">
		</form>
	</div>
</div>
	
<!-- Footer -->
<div id="footer"> Copyright 
	<img class="copyright" src="resources/Images/copyright.jpg" alt="copyright"> 2017. All Rights Reserved. <u> Website Created by Hashtec. </u> 
</div>

<!--jQuery-->
<script src="node_modules/jquery/dist/jquery.js"></script>
<script src="node_modules/bootstrap/dist/js/bootstrap.js"></script>
<script src="node_modules/leaflet/dist/leaflet.js"></script>
<script src="resources/loud-links-master/loudlinks.min.js"></script>
<script>
// Leaflet
var mymap = L.map('mapId').setView([-26.859823, 26.631750], 13);
var marker = L.marker([-26.859823, 26.631750]).addTo(mymap);

L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
    attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="http://mapbox.com">Mapbox</a>',
    maxZoom: 18,
    id: 'mapbox.satellite',
    accessToken: 'pk.eyJ1IjoiZGFuZWxsZSIsImEiOiJjajRmOTU2enMxOWg0MzNwaG9qbXEzazIwIn0.LYETS4Zb12k-MfvFZPtVJg'
}).addTo(mymap);

marker.bindPopup("<b>Hello!</b><br>This is where to find My Petshop.").openPopup();
</script>
<script src="resources/Js/Scriptsheet.js"></script>

</body> <!--end of body-->

</html>