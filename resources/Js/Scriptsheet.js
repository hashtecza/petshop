// Modal
$(document).ready(function () {
    $(".loader").attr("src", "resources/Images/th.jpg");

    $('div[data-toggle="modal"]').click(function () {
	    var modal_body = $(this).children(".product").children(".caption").text();
	    var modal_title = $(this).children(".product").children("b").text();
	    var modal_footer = $(this).children(".product").children(".product_price").text();
	    var is_wishlist = $(this).children(".product").attr('data-wishlist');

	    if (is_wishlist != null) { 
	    	$('.wishlist .fa').addClass('redheart').addClass('fa-heart').removeClass('fa-heart-o');
	    } else {
	    	$('.wishlist .fa').removeClass('redheart').removeClass('fa-heart').addClass('fa-heart-o');
	    }

	    var product_id = $(this).children('.product').attr('id');
	    $(".modal-content").attr('id', product_id.slice(product_id.indexOf("-") + 1))
		$(".modal-title").text(modal_title);
		$(".modal-body").children("p").text(modal_body);
		$(".modal-footer").text(modal_footer);
	});

    $(".heading1").each(function () {
        $(this).attr("data-heading1", $(this).text());
    });
});

// Checkout
$(".checkout").click(function() {
	$(this).children('.fa').toggleClass("bluecart");
});