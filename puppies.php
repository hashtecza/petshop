<?php
	ini_set('display_errors', 'On');
	error_reporting(E_ALL);
	include_once('boot.php');
	
	$user = $sentinel::check();
	
    include 'app/functions.php';
    include 'app/products/animals/retrieve_dogs_types_puppies.php' ;

    $apptitle = "puppy";

    $appcontent = "
	<div class=\"container\"> 
	<h2 class=\"heading2\"> 
		<img class=\"pet-icons\" src=\"resources/images/dog_paw.png\" alt=\"pet-icons\"> Puppy Food 
		<img class=\"pet-icons\" src=\"resources/images/dog_paw.png\" alt=\"pet-icons\"> 
	</h2>

	<div class=\"row\">
		" . addRetrievedProducts($q) . "
	</div>
	</div>


<!-- Modal -->
<div class=\"modal fade\" id=\"myModal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\">
	<div class=\"modal-dialog\" role=\"document\">
		<div class=\"modal-content\">
			<div class=\"modal-header\">
				<button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>
				<h4 class=\"modal-title\" id=\"myModalLabel\">Product Information</h4>
			</div>
			<div class=\"modal-body\">
				<img class=\"modal-image\" src=\"resources/Images/th.jpg\" alt=\"modal-image\">
				<p> 
					Phasellus nec scelerisque erat. Pellentesque vitae ex sit amet libero condimentum gravida. Nunc vitae orci vitae arcu eleifend rutrum. Quisque vel hendrerit dolor. Suspendisse a quam tellus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Nullam sit amet egestas tortor, at mattis est. Aenean eleifend placerat erat. Maecenas sit amet cursus augue.
				</p>
				<div class=\"wishlist-checkout-box\">
					" . getWishlist() . "
					" . getCheckout() . "
				</div>
			</div>
			<div class=\"modal-footer\">
			</div>
		</div>
	</div>
</div>
";

	$appscripts = "
		<script>
			" . getWishlistJavascript() . "
		</script>
	";

	include "layout.php";
