<?php
	ini_set('display_errors', 'On');
	error_reporting(E_ALL);

    $apptitle = "home";

    $appstyles = '
    	<link type="text/css" rel="Stylesheet" href="resources/css/stylesheet.css">
    ';

    $appcontent = "
    
	";

	$appscripts = "
		<!--JavaScript-->
		<script src=\"node_modules/jquery/dist/jquery.js\"></script>
		<script src=\"node_modules/bootstrap/dist/js/bootstrap.js\"></script>
		<script src=\"resources/loud-links-master/loudlinks.min.js\"></script>
		<script src=\"resources/js/scriptsheet.js\"></script>
	";

	include "layout.php";
?>